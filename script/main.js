$(window).ready(function() {

        $("#content-1").fadeIn(4000);
        $("#facebook").mouseover(function() {
            $(this).removeClass('zoomInDown delay-1s fast').addClass('heartBeat fast')
        });
        $('#facebook').mouseout(function() {
            $(this).removeClass("animated heartBeat fast")
        });
        $("#linkedin").mouseover(function() {
            $(this).removeClass('zoomInDown delay-2s fast').addClass('heartBeat fast')
        });
        $('#linkedin').mouseout(function() {
            $(this).removeClass("animated heartBeat fast")
        });

        $("#github").mouseover(function() {
            $(this).removeClass('zoomInDown delay-3s fast').addClass('heartBeat fast')
        });
        $('#github').mouseout(function() {
            $(this).removeClass("animated heartBeat fast")
        });

    },
    $(window).scroll(function() {
        if ($(window).scrollTop() > 500) {
            $('#inner-content-1').removeClass('.hidden').addClass('animated fadeInLeft visible');
            $('#left-side-text').addClass('animated fadeInLeft visible');
            $('#inner-content-2').removeClass('.hidden').addClass('visible animated fadeInUp delay-2s');
            $('#skill1').removeClass('.hidden').addClass('visible animated flipInX delay-3s');
            $('#skill2').removeClass('.hidden').addClass('visible animated flipInX delay-4s');
            $('#skill3').removeClass('.hidden').addClass('visible animated flipInX delay-5s');
            $('#skill4').removeClass('.hidden').addClass('visible animated flipInX');
            $('#skill5').removeClass('.hidden').addClass('visible animated flipInX');
            $('#skill6').removeClass('.hidden').addClass('visible animated flipInX');
            $('#skill7').removeClass('.hidden').addClass('visible animated flipInX');
        }
        if ($(window).scrollTop() > 950) {
            $('#facebook').addClass('animated zoomInDown delay-1s fast visible');
            $('#linkedin').addClass('animated zoomInDown delay-2s fast visible');
            $('#github').addClass('animated zoomInDown delay-3s fast visible');
        }
    }));